import React from 'react';
import { View} from 'react-native';
import {Header, Title, Left, Right, Body, Icon , Button} from 'native-base';

const HeaderCustom = (props) => {
    return (
        <Header>
            <Left>
                <Button transparent onPress={() => props.navigation.openDrawer()} title="menu" >
                  <Icon name='menu' />
                </Button>
            </Left>
            <Body>
              <Title>{props.headerText}</Title>
              <View>
              </View>
            </Body>
            <Right />
        </Header>
    );
};

export default HeaderCustom;