import React from "react";
import { AppRegistry, Image, StatusBar } from "react-native";
import { Container, Content, Text, List, ListItem, Left, Right } from "native-base";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const routes = ["Новини", "Събития", "Промоции", "Продукти", "НашатаИстория", "Локации"];
export default class SideBar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Image source={require('./../../img/sidebar-title-image.jpg')}
            style={
              {
                width:280,
                height: 120,
                alignSelf: "stretch",
                justifyContent: "center",
                alignItems: "center"
              }
            } 
          />
          <List>
            <ListItem 
                button
                onPress={() => this.props.navigation.navigate( 'Новини' )}
            >
                <Left>
                  <Icon name="newspaper" size={22} color='#555' />
                  <Text style={styles.listText}>Новини и Промоции</Text>
                </Left>
                <Right>
                  <Icon name="arrow-right" size={22} color='#ccc' />
                </Right>
            </ListItem>
            <ListItem 
                button
                onPress={() => this.props.navigation.navigate( 'Събития' )}
            >
                <Left>
                  <Icon name="calendar" size={22} color='#555' />
                  <Text style={styles.listText}>Събития</Text>
                </Left>
                <Right>
                  <Icon name="arrow-right" size={22} color='#ccc' />
                </Right>
            </ListItem>
            <ListItem 
                button
                onPress={() => this.props.navigation.navigate( 'Партньори' )}
            >
                <Left>
                  <Icon name="information" size={22} color='#555' />
                  <Text style={styles.listText}>Партньори</Text>
                </Left>
                <Right>
                  <Icon name="arrow-right" size={22} color='#ccc' />
                </Right>
            </ListItem>
            <ListItem 
                button
                onPress={() => this.props.navigation.navigate( 'НашатаИстория' )}
            >
                <Left>
                  <Icon name="domain" size={22} color='#555' />
                  <Text style={styles.listText}>За Нас</Text>
                </Left>
                <Right>
                  <Icon name="arrow-right" size={22} color='#ccc' />
                </Right>
            </ListItem>
            <ListItem 
                button
                onPress={() => this.props.navigation.navigate( 'Локации' )}
            >
                <Left>
                  <Icon name="map-marker" size={22} color='#555' />
                  <Text style={styles.listText}>Локации</Text>
                </Left>
                <Right>
                  <Icon name="arrow-right" size={22} color='#ccc' />
                </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );

  }
}

const styles = {
  listText: {
      marginLeft: 10
  }, 
}
