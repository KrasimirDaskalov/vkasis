import React, { Component, useEffect } from 'react';
import {ScrollView} from 'react-native';
import HistoryDetail from './HistoryDetail';

class History extends Component {    
    static navigationOptions = {
        title: 'Продукти',
        /* No more header config here! */
    };

    state = { history: [] };
    componentDidMount(){
        fetch('https://directus.skyeystudio.com/test/public/_/items/our_history?fields=*.*')
            .then(response => response.json())
            .then(data => this.setState({ history: data.data}));
    }

    renderHistory() {
        return this.state.history.map(history=>
            <HistoryDetail key={history.partner_name} history={history} navigation={this.props.navigation} />
        );
    }

    render() {
        return (
            <ScrollView>
                {this.renderHistory()}
            </ScrollView>
        );
    }
}

export default History;