import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './../../../styles'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import History from './History';


const AppNavigator = createStackNavigator({
    History: {
        screen: History,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Нашата История",
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#DC143C'
            },
            headerLeft: (
                <TouchableOpacity
                    style={Styles.headerButton}
                    onPress={() => navigation.openDrawer()}>
                    <Icon name="bars" size={22} color='#fff' />
                </TouchableOpacity>
            ),
        })
    }
},
);
  
export default createAppContainer(AppNavigator);