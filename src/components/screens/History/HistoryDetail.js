import React from 'react';
import {Image, View} from 'react-native';
import { Card, CardItem, Title, Body, Text, Button, H2} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import HTML from 'react-native-render-html';


const HistoryDetail = ( { history, navigation } ) => {
    const { title, description, image_one, image_two, name_image_one, name_image_two} = history;
    return (
        <View style={styles.viewStyle}>
            <Image source={require('./../../../img/sidebar-title-image.jpg')}
                style={
                {
                    height: 200,
                    flex:1,
                    width:null,
                    alignSelf: "stretch",
                    justifyContent: "center",
                    alignItems: "center"
                }
                } 
            />
            <Title style={styles.titleStyle}>{title}</Title>
            <HTML  style={styles.textStyle} html={description} />

            <Title style={styles.titleStyle}>Основатели</Title>

            <Grid style={styles.gridStyle}>
                <Col style={styles.colLeftStyle}>
                    <Image source={{uri: image_one.data.full_url }}
                        style={
                        {
                            height: 200,
                            alignSelf: "stretch",
                            justifyContent: "center",
                            alignItems: "center"
                        }
                        } 
                    />
                    <H2 style={styles.headingStyle}>{name_image_one}</H2>
                </Col>
                <Col style={styles.colRightStyle}>
                    <Image source={{uri: image_two.data.full_url }}
                        style={
                        {
                            height: 200,
                            alignSelf: "stretch",
                            justifyContent: "center",
                            alignItems: "center"
                        }
                        } 
                    />
                    <H2 style={styles.headingStyle}>{name_image_two}</H2>
                </Col>
            </Grid>
            
        </View>
    )
};

const styles = {
    viewStyle: {
        paddingLeft:15,
        paddingRight:15,
    },
    textStyle: {
        paddingBottom:30,
        borderBottomColor: '#d6d7da',
        borderBottomWidth: 2,
    },
    colLeftStyle: {
        flex: 1,
        marginLeft:5,
        marginRight: 5,
        marginTop:10,
        marginBottom:10,
        height: 200,
    }, 
    colRightStyle: {
        marginLeft:15,
        marginRight: 5,
        marginTop:10,
    },
    textStyle : {
        fontSize : 20,
    },
    headingStyle: {
        textAlign: 'center',
    },
    titleStyle: {
        fontSize : 23,
        color: '#333'
    }, 

}
export default HistoryDetail;