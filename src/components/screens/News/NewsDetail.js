import React from 'react';
import {Image} from 'react-native';
import { Card, CardItem, Title, Body, Icon, Text, Button} from 'native-base';
import HTML from 'react-native-render-html';

const NewsDetail = ( { news, navigation } ) => {
    const { title, content, image, description} = news;
    return (
        <Card padder style={styles.cardStyle}>
            <CardItem cardBody>
                <Image source={{uri:  image.data.full_url }}
                    style={styles.imageStyle} />
            </CardItem>
            <CardItem bordered>
                <Body>
                    <Title style={styles.titleStyle}>{title}</Title>
                </Body>
            </CardItem>
            <CardItem bordered>
                <Body>
                    <HTML html={content} />
                </Body>
            </CardItem>
            <CardItem  bordered>
                <Button iconRight primary   
                    onPress={() => {
                        navigation.navigate('NewsSingle', {
                        title: title,
                        image:  image.data.full_url,
                        article: description,
                        });
                    }}    
                >
                    <Text> Прочети новина </Text>
                    <Icon name='arrow-forward' />
                </Button>
            </CardItem>
        </Card>
    )
};

const styles = {
    titleStyle: {
        fontSize : 18,
        color: '#333'
    }, 
    imageStyle: {
        height: 250,
        flex: 1,
        width: 499
    },
    textStyle : {
        fontSize:14
    },
    cardStyle : {
        marginLeft:5,
        marginRight: 5,
        marginTop:10
    }

}
export default NewsDetail;