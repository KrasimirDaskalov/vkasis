import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './../../../styles'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import News from './News';
import NewsSingle from './NewsSingle';


const AppNavigator = createStackNavigator({
    News: {
        screen: News,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Новини и Промоции",
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#DC143C'
            },
            headerLeft: (
                <TouchableOpacity
                    style={Styles.headerButton}
                    onPress={() => navigation.openDrawer()}>
                    <Icon name="bars" size={22} color='#fff' />
                </TouchableOpacity>
            ),
        })
    },
    NewsSingle: {
        screen: NewsSingle,
        navigationOptions: {
            headerTitle: "Върни се назад",
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#b20f0a'
            },
        }
    },
},
);
  
export default createAppContainer(AppNavigator);