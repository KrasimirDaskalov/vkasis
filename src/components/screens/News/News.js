import React, { Component, useEffect } from 'react';
import {ScrollView, Button} from 'react-native';
import { Container,  Content } from 'native-base';
import NewsDetail from './NewsDetail';
import Notifications from "./../../../../Notification";
import NotificationSegment from "./../../NotificiationSegment"

class News extends Component {    
    static navigationOptions = {
        title: 'Home',
        /* No more header config here! */
    };

    state = { news: [] };
    
    componentDidMount(){
        fetch('https://directus.skyeystudio.com/test/public/_/items/news?fields=*.*,image.*')
            .then(response => response.json())
            .then(data => this.setState({ news: data.data}));
    }

    renderNews() {
        return this.state.news.map(news=>
            <NewsDetail key={news.title} news={news} navigation={this.props.navigation} />
        );
    }

    render() {
        return (
            <ScrollView>
		        <NotificationSegment />
                {this.renderNews()}
            </ScrollView>
        );
    }
}

export default News;