import React, {Comment} from 'react';
import {Image} from 'react-native';
import { Card, CardItem, Title, Body, Text, Container,Content} from 'native-base';
import { ScrollView, Dimensions } from 'react-native';
import HTML from 'react-native-render-html';

class EventsSingle extends React.Component {
    render() {
      const { navigation } = this.props;

      var imgurl = navigation.getParam('image', 'error');
      return (
            <Container  style={styles.containerStyle} >
                <Content>
                    <Text style={styles.titleStyle}> { navigation.getParam('title', 'error')} </Text>
                    <Image source={{uri: imgurl }}
                    style={styles.imageStyle} />
                    <HTML html={navigation.getParam('article', 'error')} imagesMaxWidth={Dimensions.get('window').width} />
                </Content>
            </Container>
      );
    }
  }

const styles = {
    containerStyle : {
        marginLeft:10,
        marginRight: 10,
        marginTop:10
    },
    titleStyle: {
        fontSize : 25,
        color: '#333',
        marginBottom: 10
    }, 
    imageStyle: {
        height: 250,
        flex: 1,
        width: null,
        borderWidth: 1,
        borderColor: '#d6d7da',
        padding:10
    },

}

export default EventsSingle;
