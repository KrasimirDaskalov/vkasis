import React, { Component, useEffect } from 'react';
import {ScrollView} from 'react-native';
import EventsDetail from './EventsDetail';

class Events extends Component {    
    static navigationOptions = {
        title: 'Events',
        /* No more header config here! */
    };

    state = { events: [] };
    componentDidMount(){
        fetch('https://directus.skyeystudio.com/test/public/_/items/events?fields=*.*,image.*')
            .then(response => response.json())
            .then(data => this.setState({ events: data.data}));
    }

    renderEvents() {
        return this.state.events.map(events=>
            <EventsDetail key={events.title} events={events} navigation={this.props.navigation} />
        );
    }

    render() {
        return (
            <ScrollView>
                {this.renderEvents()}
            </ScrollView>
        );
    }
}

export default Events;