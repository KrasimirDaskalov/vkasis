import React, { Component, useEffect } from 'react';
import {ScrollView} from 'react-native';
import ProductsDetail from './ProductsDetail';

class Products extends Component {    
    static navigationOptions = {
        title: 'Продукти',
        /* No more header config here! */
    };

    state = { products: [] };
    componentDidMount(){
        fetch('https://directus.skyeystudio.com/test/public/_/items/products?fields=*.*,')
            .then(response => response.json())
            .then(data => this.setState({ products: data.data}));
    }

    renderProducts() {
        return this.state.products.map(products=>
            <ProductsDetail key={products.partner_name} products={products} navigation={this.props.navigation} />
        );
    }

    render() {
        return (
            <ScrollView>
                {this.renderProducts()}
            </ScrollView>
        );
    }
}

export default Products;