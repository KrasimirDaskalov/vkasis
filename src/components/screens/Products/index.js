import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './../../../styles'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Products from './Products';


const AppNavigator = createStackNavigator({
    Products: {
        screen: Products,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Партньори",
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#DC143C'
            },
            headerLeft: (
                <TouchableOpacity
                    style={Styles.headerButton}
                    onPress={() => navigation.openDrawer()}>
                    <Icon name="bars" size={22} color='#fff' />
                </TouchableOpacity>
            ),
        })
    }
},
);
  
export default createAppContainer(AppNavigator);