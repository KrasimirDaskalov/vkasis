import React from 'react';
import {Image, View} from 'react-native';
import { Card, CardItem, Title, Body, Text, Button} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

const ProductsDetail = ( { products, navigation } ) => {
    const { partner_name, partner_logo} = products;
    return (
        <View>
            <Grid style={styles.gridStyle}>
                <Col style={styles.colLeftStyle}><Image source={{uri:  partner_logo.data.full_url }}
                    style={styles.imageStyle} /></Col>
                <Col style={styles.colRightStyle}>
                    <Text style={styles.textStyle}>{partner_name}</Text>
                </Col>
            </Grid>
        </View>
    )
};

const styles = {
    gridStyle: {
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },
    colLeftStyle: {
        flex: 1,
        marginLeft:5,
        marginRight: 5,
        marginTop:10,
        marginBottom:10,
        height: 200,
    }, 
    imageStyle: {
        height: 200,
        flex:1,
        width: null,
        borderRadius: 14,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },
    colRightStyle: {
        marginLeft:15,
        marginRight: 5,
        marginTop:90,
    },
    textStyle : {
        fontSize : 20,

    }

}
export default ProductsDetail;