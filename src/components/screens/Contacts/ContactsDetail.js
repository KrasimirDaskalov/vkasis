import React from 'react';
import {Image, View} from 'react-native';
import { Card, CardItem, Title, Body, Text, Button, H2} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import HTML from 'react-native-render-html';


const ContactsDetail = ( { contacts, navigation } ) => {
    const { location_name, location_image, location_description} = contacts;
    return (
        <View style={styles.viewStyle}>
            <Grid style={styles.gridStyle}>
                <Col style={styles.colLeftStyle}>
                    <Image source={{uri: location_image.data.full_url }}
                        style={
                        {
                            height: 200,
                            alignSelf: "stretch",
                            justifyContent: "center",
                            alignItems: "center"
                        }
                        } 
                    />
                </Col>
                <Col style={styles.colRightStyle}>
                    <H2 style={styles.headingStyle}>{location_name}</H2>
                    <HTML  style={styles.textStyle} html={location_description} />
                </Col>
            </Grid>
        </View>
    )
};

const styles = {
    viewStyle: {
        paddingLeft:15,
        paddingRight:15,
    },
    colLeftStyle: {
        flex: 1,
        marginLeft:5,
        marginRight: 5,
        marginTop:10,
        marginBottom:10,
        height: 200,
    }, 
    colRightStyle: {
        marginLeft:5,
        marginRight: 5,
        marginTop:10,
    },
    textStyle : {
        fontSize : 14,
        lineHeight:10
    },
    headingStyle: {
        textAlign: 'left',
        fontWeight: 'bold',
        fontSize: 19
    },
    gridStyle : {
        borderBottomColor: '#d6d7da',
        borderBottomWidth: 1,
    }

}
export default ContactsDetail;