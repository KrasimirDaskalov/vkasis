import React, { Component, useEffect } from 'react';
import {ScrollView} from 'react-native';
import {Title} from 'native-base';
import ContactsDetail from './ContactsDetail';

class Contacts extends Component {    
    static navigationOptions = {
        title: 'Контакти',
        /* No more header config here! */
    };

    state = { contacts: [] };
    componentDidMount(){
        fetch('https://directus.skyeystudio.com/test/public/_/items/locations?fields=*.*')
            .then(response => response.json())
            .then(data => this.setState({ contacts: data.data}));
    }

    renderContacts() {
        return this.state.contacts.map(contacts=>
            <ContactsDetail key={contacts.partner_name} contacts={contacts} navigation={this.props.navigation} />
        );
    }

    render() {
        return (
            <ScrollView>
                <Title style={styles.titleStyle}>Нашите Складове</Title>
                {this.renderContacts()}
            </ScrollView>
        );
    }
}

const styles = {
    titleStyle: {
        fontSize : 25,
        textAlign: 'left',
        marginLeft:20,
        color: '#333',
        marginTop:15,
        marginBottom:10
    }, 

}

export default Contacts;