import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './../../../styles'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Contacts from './Contacts';


const AppNavigator = createStackNavigator({
    Contacts: {
        screen: Contacts,
        navigationOptions: ({navigation}) => ({
            headerTitle: "Локации",
            headerTintColor: 'white',
            headerStyle: {
                backgroundColor: '#DC143C'
            },
            headerLeft: (
                <TouchableOpacity
                    style={Styles.headerButton}
                    onPress={() => navigation.openDrawer()}>
                    <Icon name="bars" size={22} color='#fff' />
                </TouchableOpacity>
            ),
        })
    }
},
);
  
export default createAppContainer(AppNavigator);