import React, { Component, useEffect} from 'react';
import { SafeAreaView, StyleSheet, View, Text, Button } from "react-native"
import { } from 'native-base';
import Notifications from "./../../Notification";

// export default AppContainer;
export const NotificationSegment = () => {
  useEffect(() => {
    Notifications.configure()
  }, [])

  Notifications.wasOpenedByNotification()

  const sendLocalNotification = delay_seconds => {
    const date = new Date()
    date.setSeconds(date.getSeconds() + delay_seconds)
    Notifications.scheduleNotification(date)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    padding: 8,
  },
})

export default NotificationSegment