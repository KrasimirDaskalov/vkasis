import React, { Component, useEffect} from 'react';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import News from './src/components/screens/News';
import Events from './src/components/screens/Events';
import Products from './src/components/screens/Products';
import History from './src/components/screens/History';
import Contacts from './src/components/screens/Contacts';
import Icon from 'react-native-vector-icons/FontAwesome5';


import SideBar from "./src/components/common/Sidebar";

 const MyDrawerNavigator = createDrawerNavigator({
   Новини: {
     screen: News,
   },
   Събития: {
     screen: Events,
   },
   Партньори: {
    screen: Products,
   },
   НашатаИстория: {
    screen: History,
   },
   Локации: {
    screen: Contacts,
   },
 },
 {
  initialRouteName: 'Новини',
  contentComponent: props => <SideBar {...props} />
 }
);

 const AppContainer = createAppContainer(MyDrawerNavigator);


export default AppContainer;